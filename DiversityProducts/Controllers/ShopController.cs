﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace DiversityProducts.Controllers
{
    public class ShopController : Controller
    {
        public ActionResult Category()
        {
            return View();
        }
        public ActionResult ProductDetails()
        {
            return View();
        }
        public ActionResult ProductCheckout()
        {
            return View();
        }
        public ActionResult ShoppingCart()
        {
            return View();
        }

    }
}